from django import forms

from .models import Bird, Dog, Cat


class CreateBirdForm(forms.ModelForm):
    class Meta:
        model = Bird
        fields = ['race', 'sound']
        widgets = {
            'race': forms.HiddenInput(),
            'sound': forms.HiddenInput()
        }


class CreateDogForm(forms.ModelForm):
    class Meta:
        model = Dog
        fields = ['animal02']
        widgets = {'animal02': forms.HiddenInput()}


class CreateCatForm(forms.ModelForm):
    class Meta:
        model = Cat
        fields = ['animal03']
        widgets = {'animal03': forms.HiddenInput()}



