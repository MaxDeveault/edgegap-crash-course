import datetime

from django.db import models
from django.utils import timezone


# Create your models here.
class Animals(models.Model):
    race = models.CharField(max_length=200)


class Bird(models.Model):
    race = models.CharField(max_length=200, default='Bird')
    sound = models.CharField(max_length=200, default='')

    def test_sound(self):
        if self.race == 'Bird':
            self.sound == 'Tweet'

        elif self.race == 'Dog':
            self.sound == 'Woof'

        elif self.race == 'cat':
            self.sound == 'Meow'

        return self.sound


class Dog(models.Model):
    animal02 = models.CharField(max_length=200, default='Dog')


class Cat(models.Model):
    animal03 = models.CharField(max_length=200, default='Cat')


def make_sound(race):
    sound = " "
    if race == 'Bird':
        sound == 'Tweet'

    elif race == 'Dog':
        sound == 'Woof'

    elif race == 'cat':
        sound == 'Meow'



