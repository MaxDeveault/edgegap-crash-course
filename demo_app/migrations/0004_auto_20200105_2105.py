# Generated by Django 3.0.1 on 2020-01-06 02:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('demo_app', '0003_animals'),
    ]

    operations = [
        migrations.RenameField(
            model_name='bird',
            old_name='animal01',
            new_name='race',
        ),
        migrations.AddField(
            model_name='bird',
            name='sound',
            field=models.CharField(default='', max_length=200),
        ),
    ]
