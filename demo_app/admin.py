from django.contrib import admin

from.models import Bird, Dog, Cat, Animals

admin.site.register(Bird)
admin.site.register(Dog)
admin.site.register(Cat)
admin.site.register(Animals)
