from django.shortcuts import render

from django.http import HttpResponse

from .models import Bird, Cat, Dog, Animals
from django.views.generic import CreateView
from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin
from .forms import CreateBirdForm, CreateCatForm, CreateDogForm
from django.template import loader

# Create your views here.


def index(request):
    animals_created_list1 = Bird.objects.order_by('id')
    animals_created_list2 = Dog.objects.order_by('id')
    animals_created_list3 = Cat.objects.order_by('id')
    template = loader.get_template('demo_app/index.html')
    context = {
        'animals_created_list1': animals_created_list1,
        'animals_created_list2': animals_created_list2,
        'animals_created_list3': animals_created_list3,
    }
    return HttpResponse(template.render(context, request))


def cat_create_view(request):
    form = CreateCatForm(request.POST or None)
    if form.is_valid():
        form.save()
    context = {
        'form': form
    }
    return render(request, 'demo_app/cat.html', context)


def dog_create_view(request):
    form = CreateDogForm(request.POST or None)
    if form.is_valid():
        form.save()
    context = {
        'form': form
    }
    return render(request, 'demo_app/dog.html', context)


def bird_create_view(request):
    form = CreateBirdForm(request.POST or None)
    if form.is_valid():
        form.save()
    context = {
        'form': form
    }
    return render(request, 'demo_app/index.html', context)


def animal_create_view(request):
    form_cat = CreateCatForm(request.POST)
    form_dog = CreateDogForm(request.POST)
    form_bird = CreateBirdForm(request.POST)
    if form_cat.is_valid():
        form_cat.save()
    elif form_bird.is_valid():
        form_bird.save()
    elif form_dog.is_valid():
        form_dog.save()
    context = {
        'form_cat': form_cat,
        'form_dog': form_dog,
        'form_bird': form_bird
    }
    return render(request, 'demo_app/index.html', context)


class MyView(LoginRequiredMixin, CreateView):

    model = Animals
    fields = ['race']
    # form_dog = CreateDogForm
    # form_bird = CreateBirdForm

    def get_success_url(self, form_class):
        if 'form_class' in self.request.POST:
            if form_class.is_valid():
                form_class.save()
            context = {'form_class': form_class}

        elif 'create_class' in self.request.POST:
            if form_class.is_valid():
                form_class.save()
            context = {'form_class': form_class}
        elif'create_class' in self.request.POST:
            if form_class.is_valid():
                form_class.save()
            context = {'form_class': form_class}

        return render((self.request, 'demo_app/index.html', context))
